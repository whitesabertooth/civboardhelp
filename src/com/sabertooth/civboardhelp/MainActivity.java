package com.sabertooth.civboardhelp;

import java.util.Locale;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.sabertooth.civboardhelp.R;

public class MainActivity extends FragmentActivity  
implements AddCityDialog.NoticeDialogListener, AddPlayerDialog.PlayerDialogListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionPagerAdaptor mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionPagerAdaptor(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    

	public void onDialogCityClick(int which,int position)
	{
		//Dialog callback
		//Get current fragment and send message to it
		mSectionsPagerAdapter.addCity(which,position);
	}
	
	public void onDialogPlayerClick(String player)
	{
		//Dialog callback
		//Get current fragment and send message to it
		//mSectionsPagerAdapter.instantiateItem(mViewPager, position);
		//mSectionsPagerAdapter.notifyDataSetChanged();
        
		mViewPager.setAdapter(null);
		int position = mSectionsPagerAdapter.addPlayer(player);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(position);
		
	}
	
    /**
     * A dummy fragment representing a section of the app, but that simply
     * displays dummy text.
     */
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.add_player:
               	AddPlayerDialog newFragment = new AddPlayerDialog();
                   newFragment.show(getSupportFragmentManager(), "missiles");
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
   

    
}
