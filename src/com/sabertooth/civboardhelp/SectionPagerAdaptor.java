package com.sabertooth.civboardhelp;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SectionPagerAdaptor extends FragmentPagerAdapter {

	
	List<DummySectionFragment> allfrags = new ArrayList<DummySectionFragment>();
	List<PlayerData> allData = new ArrayList<PlayerData>();
    public SectionPagerAdaptor(FragmentManager fm) {
        super(fm);
    }
    
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void addCity(int which, int position)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	//Add to database
    	int index = allData.get(position).addCity(which);
    	//Add to display
    	fragment.addCity(allData.get(position).getCity(index));
    	updateProduction(position);
    	notifyDataSetChanged();
    }

    public void changeCitySize(int position, int cityidx, int sizechg)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	//Add to database
    	allData.get(position).changeCitySize(cityidx,sizechg);
    	fragment.changeCity(allData.get(position).getCity(cityidx),cityidx);
    	updateProduction(position);
    	notifyDataSetChanged();
    }

    public void removeCity(int position, int cityidx)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	//Add to database
    	allData.get(position).removeCity(cityidx);
    	fragment.removeCity(cityidx);
    	updateProduction(position);
    	notifyDataSetChanged();
    }
    
    public void updateMoney(int money, int position)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	//Add to database
    	int index = allData.get(position).changeMoney(money);
    	//Add to display
    	fragment.setMoney(index);
    	notifyDataSetChanged();
    }
    
    public void updateProduction(int position)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	//Add to display
    	fragment.setProduction(allData.get(position).getProduction());
    	notifyDataSetChanged();
    }
    
    public void changeBonus(int bonus, int position)
    {
    	allData.get(position).setBonus(bonus);
    	updateProduction(position);
    	notifyDataSetChanged();
    }
    
    public void addHappy(int position,int add)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	allData.get(position).addHappy(add);
    	fragment.setHappy(allData.get(position).getHappy());
    	notifyDataSetChanged();
    }
    
    public void applyProduction(int position)
    {
    	updateMoney(allData.get(position).getProduction(),position);
    }
    

    public void addProdPoints(int position,int add)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	allData.get(position).addNumProduction(add);
    	fragment.setProdPoints(allData.get(position).getNumProduction());
    	notifyDataSetChanged();
    }
    
    public void updateAllData(int position)
    {
    	DummySectionFragment fragment = allfrags.get(position);
    	fragment.setBonus(allData.get(position).getBonus());
    	fragment.setProduction(allData.get(position).getProduction());
    	fragment.setMoney(allData.get(position).getMoney());
    	fragment.setProdPoints(allData.get(position).getNumProduction());
    	fragment.setHappy(allData.get(position).getHappy());
    	//Update all cities
    	int max = allData.get(position).getCitySize();
    	//check for discrepancy on number of cities
    	if(fragment.getCitySize() == 0)
    	{
    		//Remove all old cities
    		//fragment.removeAllCities();

    		//Add all cities again
	    	for(int i = 0; i < max; i++)
	    	{
	    		if (!allData.get(position).removedCity(i))
	    		{
	    			fragment.addCity(allData.get(position).getCity(i));
	    		}
	    	}
    	} else {
    		//Just re-add the views
	    	for(int i = 0; i < max; i++)
	    	{
	    		if (!allData.get(position).removedCity(i))
	    		{
	    			fragment.refreshCity(i);
	    		}
	    	}
    	}
    }
    
    
    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a DummySectionFragment (defined as a static inner class
        // below) with the page number as its lone argument.
    	
    	DummySectionFragment fragment = new DummySectionFragment();
    	fragment.setPosition(position);
    	allfrags.add(position,fragment);
    	fragment.setAdapter(this);
        
    	//Bundle args = new Bundle();
        //args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
        //fragment.setArguments(args);
    	
        return fragment;
    }

    public int addPlayer(String player)
    {
    	PlayerData newplayer = new PlayerData(player);
    	allData.add(newplayer);
    	//Adds new tab
    	//startUpdate(null);
    	return allData.size();
    	
    }
    
    @Override
    public int getCount() {
        // Show X total pages.
        return allData.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        return allData.get(position).getName();
    }
}