package com.sabertooth.civboardhelp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PlayerData {

	public static enum  citytype { OIL,WINE,RARE_MATERIALS,GEMS,HORSE,COAL,SPICES,IRON,FERTILE,NORMAL}
	public final citytype LAST_SPECIAL = citytype.IRON;
   	private class CityData {
   		private citytype type;
   		private int citysize;
   		private boolean removed;
   		public citytype getType()
   		{
   			return type;
   		}
   		public int getSize()
   		{
   			return citysize;
   		}
   		public void removeCity() { removed = true; }
   		public boolean removed() { return removed; }
   		CityData(citytype newtype) { citysize = 1; type = newtype; removed = false;}  
   		CityData(int newtype) {
   			citysize = 1;
   			if(newtype == citytype.OIL.ordinal())
   				type = citytype.OIL;
   			else if(newtype == citytype.WINE.ordinal())
   				type = citytype.WINE;
   			else if(newtype == citytype.RARE_MATERIALS.ordinal())
   				type = citytype.RARE_MATERIALS;
   			else if(newtype == citytype.GEMS.ordinal())
   				type = citytype.GEMS;
   			else if(newtype == citytype.HORSE.ordinal())
   				type = citytype.HORSE;
   			else if(newtype == citytype.COAL.ordinal())
   				type = citytype.COAL;
   			else if(newtype == citytype.SPICES.ordinal())
   				type = citytype.SPICES;
   			else if(newtype == citytype.IRON.ordinal())
   				type = citytype.IRON;
   			else if(newtype == citytype.FERTILE.ordinal())
   			{
   				type = citytype.FERTILE;
   				citysize++;
   			}
   			else if(newtype == citytype.NORMAL.ordinal())
   				type = citytype.NORMAL;
   		}
   		public String toString()
   		{
   			String strtype = "";
   			switch(type)
   			{
   			case OIL:
   				strtype = "OIL";
   				break;
   			case WINE:
   				strtype = "WINE";
   				break;
   			case RARE_MATERIALS:
   				strtype = "RARE MATERITALS";
   				break;
   			case GEMS:
   				strtype = "GEMS";
   				break;
   			case HORSE:
   				strtype = "HORSE";
   				break;
   			case COAL:
   				strtype = "COAL";
   				break;
   			case SPICES:
   				strtype = "SPICES";
   				break;
   			case IRON:
   				strtype = "IRON";
   				break;
   			case FERTILE:
   				strtype = "FERTILE";
   				break;
   			case NORMAL:
   				strtype = "NORMAL";
   				break;
   			}
   			return strtype + " size " + citysize;
   		}
   	}

   	private List<CityData> allCities = new ArrayList<CityData>();
   	private String playerName;
   	private int money;
   	private int currentProduction;
   	private int perturnbonus;
   	private int numhappy;
   	private int numproduction;
   	public PlayerData(String player)
   	{
   		playerName = player;
   		money = 0;
   		currentProduction = 0;
   		perturnbonus = 0;
   		numhappy = 1;//base with 1 happy already
   		numproduction = 0;
   	}
   	public int getMoney() { return money; }
   	public int getHappy() { return numhappy;}
   	public int getNumProduction() { return numproduction;}
   	public void addHappy(int happy) { numhappy += happy; }
   	public void addNumProduction(int prod) { numproduction += prod; }
   	public int changeMoney(int chg) { money = money + chg; return money;}
   	public int getProduction() {
   		//Calculate production:
   		//1. City production:
   		//	- happy (gems, wine already happy)
   		//	- Productions (fertile has production already)
   		//2. Critical Resource: 15 gold each 
   		//3. Unique resource: # unique x 3
   		//4. Per turn bonus
   		//5. Monopolies:
   		//	- 3 of a kind = 20
   		//	- 4 of a kind = 40
   		//	- 5 of a kind = 80
   		
   		currentProduction = 0;
   		//1. City production:
   		// Special: H  P  S  C
   		//          N  N  1  2
   		//          N  Y  1  4
   		//          Y  N  1  4
   		//          Y  Y  1  8
   		//          N  N  2  4
   		//          N  Y  2  8
   		//          Y  N  2  6
   		//          Y  Y  2  12
   		int happy = getHappy();
   		int prod = getNumProduction();
   			//Look for largest size special (gem and wine 1st) and apply happy and production 1st 
		List<Integer> sortedCities = new ArrayList<Integer>();
		int largest = -1;
		int currentsize = 0;
		citytype currenttype = citytype.NORMAL;
		boolean foundsorted = false;
		do {
			largest = -1;
			currentsize = 0;
			currenttype = citytype.NORMAL;
			foundsorted = false;
			for(int i=0; i<allCities.size(); ++i)
			{
				if(!allCities.get(i).removed())
				{
					//Make sure i is not sorted already
					foundsorted = false;
					for(int j = 0; j<sortedCities.size(); ++j)
					{
						if(sortedCities.get(j) == i)
						{
							foundsorted = true;
							break;
						}
					}
					if(!foundsorted)
					{
						if((allCities.get(i).getType().ordinal() <= LAST_SPECIAL.ordinal() && allCities.get(i).citysize > currentsize) ||
								(allCities.get(i).citysize >= currentsize && (allCities.get(i).getType() == citytype.GEMS || allCities.get(i).getType() == citytype.WINE)))
						{
							currentsize = allCities.get(i).citysize;
							currenttype = allCities.get(i).type;
							largest = i;
						}
					}
				}
			}
			if(largest > -1)
			{
				sortedCities.add(largest);
			}
		}while(largest > -1);
   			//Look for largest size fertile next and apply happy 2nd
		do {
			largest = -1;
			currentsize = 0;
			currenttype = citytype.NORMAL;
			foundsorted = false;
			for(int i=0; i<allCities.size(); ++i)
			{
				if(!allCities.get(i).removed())
				{
					//Make sure i is not sorted already
					foundsorted = false;
					for(int j = 0; j<sortedCities.size(); ++j)
					{
						if(sortedCities.get(j) == i)
						{
							foundsorted = true;
							break;
						}
					}
					if(!foundsorted)
					{
						if(allCities.get(i).getType() == citytype.FERTILE && allCities.get(i).citysize > currentsize)
						{
							currentsize = allCities.get(i).citysize;
							currenttype = allCities.get(i).type;
							largest = i;
						}
					}
				}
			}
			if(largest > -1)
			{
				sortedCities.add(largest);
			}
		}while(largest > -1);
		
			//Look for largest size normal next and apply happy and production 3rd
		do {
			largest = -1;
			currentsize = 0;
			currenttype = citytype.NORMAL;
			foundsorted = false;
			for(int i=0; i<allCities.size(); ++i)
			{
				if(!allCities.get(i).removed())
				{
					//Make sure i is not sorted already
					foundsorted = false;
					for(int j = 0; j<sortedCities.size(); ++j)
					{
						if(sortedCities.get(j) == i)
						{
							foundsorted = true;
							break;
						}
					}
					if(!foundsorted)
					{
						if(allCities.get(i).getType() == citytype.NORMAL && allCities.get(i).citysize > currentsize)
						{
							currentsize = allCities.get(i).citysize;
							currenttype = allCities.get(i).type;
							largest = i;
						}
					}
				}
			}
			if(largest > -1)
			{
				sortedCities.add(largest);
			}
		}while(largest > -1);
		//Go through all sorted and calculated each
		int ishappy = 0;
		int isprod = 0;
		int isspecial = 0;
		int size = 0;
		for(int i = 0; i<sortedCities.size(); ++i)
		{
			ishappy = 0;
			isprod = 0;
			isspecial = 0;
			if(allCities.get(sortedCities.get(i)).getType() == citytype.GEMS ||
					allCities.get(sortedCities.get(i)).getType() == citytype.WINE)
			{
				ishappy = 1;
			} else if(happy > 0)
			{
				ishappy = 1;
				happy--;
			}
			
			if(allCities.get(sortedCities.get(i)).getType().ordinal() <= LAST_SPECIAL.ordinal())
			{
				isspecial = 1;
			}
			
			if(allCities.get(sortedCities.get(i)).getType() == citytype.FERTILE)
			{
				isprod = 1;
			} else if(prod > 0)
			{
				isprod = 1;
				prod--;
			}
			
			//Calculate
			//(Size-1+(Special))*(1+Productive)*(1+Special)+(Happy*2)*(1+Productive)
			size = allCities.get(sortedCities.get(i)).citysize;
			currentProduction += (size -1 + isspecial)*(1+isprod)*(1+isspecial)+(ishappy*2)*(1+isprod);
		}
   		//(TODO)2. Critical Resource: 15 gold each 
   		
		//3. Unique resource: # unique x 3
		List<citytype> uniqueresources = new ArrayList<citytype>();
		List<Integer> uniqueresourcessize = new ArrayList<Integer>();
		for(int i=0; i<allCities.size(); ++i)
		{
			if(!allCities.get(i).removed())
			{
				//Make sure i is not sorted already
				foundsorted = false;
				for(int j = 0; j<uniqueresources.size(); ++j)
				{
					if(uniqueresources.get(j) == allCities.get(i).getType())
					{
						foundsorted = true;
						uniqueresourcessize.set(j, uniqueresourcessize.get(j)+1);//add one
						break;
					}
				}
				if(!foundsorted)
				{
					if(allCities.get(i).getType().ordinal() <= LAST_SPECIAL.ordinal())
					{
						uniqueresources.add(allCities.get(i).getType());
						uniqueresourcessize.add(1);
					}
				}
			}
		}
		currentProduction += uniqueresources.size() * 3;
		
   		//4. Per turn bonus
		currentProduction += perturnbonus;
		
   		//5. Monopolies:
		for(int i=0; i<uniqueresourcessize.size(); ++i)
		{

	   		//	- 3 of a kind = 20
	   		//	- 4 of a kind = 40
	   		//	- 5 of a kind = 80
			if(uniqueresourcessize.get(i) == 3)
			{
				currentProduction += 20;
			}
			if(uniqueresourcessize.get(i) == 4)
			{
				currentProduction += 40;
			}
			if(uniqueresourcessize.get(i) == 5)
			{
				currentProduction += 80;
			}

		}
		
   		return currentProduction; 
   	}
   	public int getBonus() { return perturnbonus; }
   	public void setBonus(int bonus) { perturnbonus = bonus; }
   	public String getName() { return playerName; }
   	public int addCity(int type) { CityData newcity = new CityData(type); allCities.add(newcity); return allCities.size()-1;}
   	public void changeCitySize(int index,int sizechg) { if(allCities.size() > index) allCities.get(index).citysize+=sizechg; }
   	public int getCitySize() {return allCities.size(); }
   	public String getCity(int index) { if(allCities.size() <= index) return ""; else return allCities.get(index).toString(); }
   	public void removeCity(int index) { if(allCities.size() > index) allCities.get(index).removeCity(); }
   	public boolean removedCity(int index) { if(allCities.size() > index) return allCities.get(index).removed(); else return true; }
}
