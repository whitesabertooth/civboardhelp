package com.sabertooth.civboardhelp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;
import com.sabertooth.civboardhelp.R;

public class AddPlayerDialog extends DialogFragment {
	public interface PlayerDialogListener {
	    public void onDialogPlayerClick(String player);
	    //public void onDialogNegativeClick(DialogFragment dialog);
	}
	private int myposition_ = 0;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	 
	    
	    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

	    alert.setTitle(R.string.player_dialog_title);
	    alert.setMessage("Enter Name");

	    // Set an EditText view to get user input 
	    final EditText input = new EditText(getActivity());
	    alert.setView(input);

	    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialog, int whichButton) {
	      String value = input.getText().toString();
	      // Do something with value!
   	   		mListener.onDialogPlayerClick(value);
	      }
	    });

	    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	      public void onClick(DialogInterface dialog, int whichButton) {
	        // Canceled.
	      }
	    });

	    //alert.show();
	    
	    return alert.create();
	}
	
	public void setData(int position)
	{
		myposition_ = position;
	}
    // Use this instance of the interface to deliver action events
	PlayerDialogListener mListener;
    
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (PlayerDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement PlayerDialogListener");
        }
    }
}
