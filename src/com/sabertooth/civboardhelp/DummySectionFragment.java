package com.sabertooth.civboardhelp;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sabertooth.civboardhelp.R;

public  class DummySectionFragment extends Fragment{
	
   	
	private int myposition_=0;
	private List<TextView> allCityViews = new ArrayList<TextView>();
	private List<LinearLayout> allCityLayouts = new ArrayList<LinearLayout>();
	private LinearLayout citylayoutOld = null;
	private LinearLayout citylayout = null;
	private View rootView = null;
	private SectionPagerAdaptor localadapter_;
   	//private PlayerData mydata;
       /**
        * The fragment argument representing the section number for this
        * fragment.
        */
       //public static final String ARG_SECTION_NUMBER = "section_number";

	public void setPosition(int position)
	{
		myposition_ = position;
	}
       
	public void refreshCity(int i)
	{
		//Remove old link if still available
		if(citylayoutOld != null)
		{
			citylayoutOld.removeView(allCityLayouts.get(i));
		}
		citylayout.addView(allCityLayouts.get(i));
	}
	
	public void addCity(String citydata)
	{
		int pos = allCityViews.size();
		LinearLayout layout = new LinearLayout(citylayout.getContext());
		Button plusbutton = new Button(citylayout.getContext());
		Button minusbutton = new Button(citylayout.getContext());
		Button deletebutton = new Button(citylayout.getContext());
		TextView newview = new TextView(citylayout.getContext());
		
		LayoutParams lparams = new LayoutParams(
				   LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		newview.setLayoutParams(lparams);
		plusbutton.setLayoutParams(lparams);
		minusbutton.setLayoutParams(lparams);
		deletebutton.setLayoutParams(lparams);
		layout.setLayoutParams(lparams);
		
		newview.setText(citydata);
		plusbutton.setText("+");
		minusbutton.setText("-");
		deletebutton.setText("X");
		
		//Add one to size of city
		OnClickListener listener = new View.OnClickListener() {
		    private int pos_;
			public void onClick(View v) {
               	//update money
            	   localadapter_.changeCitySize(myposition_,pos_, 1);
               }
			public OnClickListener setPos(int pos)
			{
				pos_ = pos;
				return this;
			}
           }.setPos(pos);

   		//Minus one to size of city
   		OnClickListener listener2 = new View.OnClickListener() {
   		    private int pos_;
   			public void onClick(View v) {
                  	//update money
               	   localadapter_.changeCitySize(myposition_,pos_, -1);
                  }
   			public OnClickListener setPos(int pos)
   			{
   				pos_ = pos;
   				return this;
   			}
              }.setPos(pos);
 		//Minus one to size of city
 		OnClickListener listener3 = new View.OnClickListener() {
 		    private int pos_;
 			public void onClick(View v) {
                	//update money
             	   localadapter_.removeCity(myposition_,pos_);
                }
 			public OnClickListener setPos(int pos)
 			{
 				pos_ = pos;
 				return this;
 			}
            }.setPos(pos);
		plusbutton.setOnClickListener(listener);
		minusbutton.setOnClickListener(listener2);
		deletebutton.setOnClickListener(listener3);
		
		layout.addView(newview);
		layout.addView(plusbutton);
		layout.addView(minusbutton);
		layout.addView(deletebutton);
		citylayout.addView(layout);
		allCityViews.add(newview);
		allCityLayouts.add(layout);
	}
	
	public int getCitySize()
	{
		return allCityLayouts.size();
	}
	
	public void changeCity(String citydata,int index)
	{
		allCityViews.get(index).setText(citydata);
	}
	public void removeCity(int index)
	{
		citylayout.removeView(allCityLayouts.get(index));
		//allCityViews.remove(index);
		//allCityLayouts.remove(index);
	}
	public void setMoney(int money)
	{
		TextView newmoney = (TextView) rootView.findViewById(R.id.currentmoney);
		newmoney.setText(Integer.toString(money));
	}
	public void setProduction(int prod)
	{
		TextView newprod = (TextView) rootView.findViewById(R.id.production);
		newprod.setText(Integer.toString(prod));
	}
	public void setBonus(int bonus)
	{
		EditText newprod = (EditText) rootView.findViewById(R.id.perturnbonus);
		newprod.setText(Integer.toString(bonus));
	}
	

	public void setHappy(int happy)
	{
		TextView newprod = (TextView) rootView.findViewById(R.id.happy_points);
		newprod.setText(Integer.toString(happy));
	}

	public void setProdPoints(int prod)
	{
		TextView newprod = (TextView) rootView.findViewById(R.id.prod_points);
		newprod.setText(Integer.toString(prod));
	}
	
	public void setAdapter(SectionPagerAdaptor ad)
	{
		localadapter_ = ad;
	}

       @Override
       public View onCreateView(LayoutInflater inflater, ViewGroup container,
               Bundle savedInstanceState) {
           rootView = inflater.inflate(R.layout.fragment_main_dummy, container, false);
           
           citylayoutOld = citylayout;
           citylayout = (LinearLayout) rootView.findViewById(R.id.mainlayout);
          // dummyTextView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));
           final Button button = (Button) rootView.findViewById(R.id.button_addcity);
           button.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//Create Dialog
               	AddCityDialog newFragment = new AddCityDialog();
               	newFragment.setData(myposition_);
                   newFragment.show(getFragmentManager(), "missiles");
               }
           });
           localadapter_.updateAllData(myposition_);
           
           final Button button2 = (Button) rootView.findViewById(R.id.button_addmoney);
           button2.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
            	   EditText ev = (EditText) rootView.findViewById(R.id.changemoney);
            	   Editable ed = ev.getEditableText();
            	   
            	   String val = ed.toString();
            	   int money = Integer.parseInt(val);
            	   localadapter_.updateMoney(money, myposition_);
               }
           });
           final Button button3 = (Button) rootView.findViewById(R.id.button_minusmoney);
           button3.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
            	   EditText ev = (EditText) rootView.findViewById(R.id.changemoney);
            	   Editable ed = ev.getEditableText();
            	   
            	   String val = ed.toString();
            	   int money = Integer.parseInt(val);
            	   localadapter_.updateMoney(-money, myposition_);
               }
           });
           

           final Button button4 = (Button) rootView.findViewById(R.id.button_happyplus);
           button4.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
               	   localadapter_.addHappy(myposition_,1);
               }
           });

           final Button button5 = (Button) rootView.findViewById(R.id.button_happyminus);
           button5.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
               	   localadapter_.addHappy(myposition_,-1);
               }
           });

           final Button button6 = (Button) rootView.findViewById(R.id.button_prodplus);
           button6.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
               	   localadapter_.addProdPoints(myposition_,1);
               }
           });

           final Button button7 = (Button) rootView.findViewById(R.id.button_prodminus);
           button7.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
               	   localadapter_.addProdPoints(myposition_,-1);
               }
           });
           
           final Button button8 = (Button) rootView.findViewById(R.id.button_apply_prod);
           button8.setOnClickListener(new View.OnClickListener() {
               public void onClick(View v) {
               	//update money
               	   localadapter_.applyProduction(myposition_);
               }
           });
           
           final EditText edittext = (EditText) rootView.findViewById(R.id.perturnbonus);
           edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
		     	//update money
				if(hasFocus == false)
         	   {
					EditText ev = (EditText) rootView.findViewById(R.id.perturnbonus);
	        	   Editable ed = ev.getEditableText();
	        	   
	        	   String val = ed.toString();
	        	   int bonus = Integer.parseInt(val);
	         	   localadapter_.changeBonus(bonus, myposition_);
	         	   //TODO: Fix bug about changing this by focus
         	   }
       			}
		}); return rootView;
       }
   }
